import fs from "fs-extra";
import path from "path";
import yaml from "js-yaml";
import colors from "colors";
import prompts from "prompts";

console.log(colors.green("Hello!"));

async function main() {
  const points_part = [0];
  const outcome = { grade: "" };

  points_part[1] = (await prompts({
    type: "number",
    name: "value",
    message: "Poäng på Del I?",
    initial: 0
  })).value;

  points_part[2] = (await prompts({
    type: "number",
    name: "value",
    message: "Poäng på Del II?",
    initial: 0
  })).value;

  points_part[3] = (await prompts({
    type: "number",
    name: "value",
    message: "Poäng på Del III?",
    initial: 0
  })).value;

  let result = "";

  points_part[0] = points_part[1] + points_part[2] + points_part[3];
  const high_points = points_part[2] + points_part[3];

  outcome.achiment = [false, false, false, false];

  if (points_part[3] === 4) {
    outcome.achiment[3] = true;
  }

  if (points_part[2] === 6) {
    outcome.achiment[2] = true;
  }

  if (points_part[1] === 14) {
    outcome.achiment[1] = true;
  }

  if (points_part[0] === 24) {
    outcome.achiment[0] = true;
  }

  if (points_part[0] >= 19 && points_part[3] >= 2) {
    outcome.grade = "A";
  } else if (points_part[0] >= 17 && points_part[3] >= 1) {
    outcome.grade = "B";
  } else if (points_part[0] >= 14 && high_points >= 2) {
    outcome.grade = "C";
  } else if (points_part[0] >= 12 && high_points >= 1) {
    outcome.grade = "D";
  } else if (points_part[0] >= 10) {
    outcome.grade = "E";
  } else {
    outcome.grade = "F";
  }
  result +=
    "\nResult: " +
    points_part[1] +
    "/" +
    points_part[2] +
    "/" +
    points_part[3] +
    "\nTotal:  " +
    points_part[0];

  result += "\n" + outcome.grade + "\n";

  const medals = ["💎", "🥉", "🥈", "🥇"];

  for (let i = 0; i < outcome.achiment.length; i++) {
    if (outcome.achiment[i]) {
      result += medals[i];
    }
  }
  result += "\n";

  switch (outcome.grade) {
    case "A":
      result += "😂 Perfekt!";
      break;
    case "C":
    case "B":
      result += "😁 Fantastiskt!";
      break;
    case "E":
    case "D":
      result += "🙂 Bra!";
      break;
    default:
      result += "🙄 Du får försöka igen!\n";
      result += "Det är inte fel att få F, det är mycket värre att fuska!";
  }

  //  Good for debuging program
  //  console.log(outcome);
  console.log(result);
}

main();
